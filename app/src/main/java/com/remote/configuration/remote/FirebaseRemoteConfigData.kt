package com.remote.configuration.remote

import android.util.Log
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.gson.Gson
import com.remote.configuration.BuildConfig

class FirebaseRemoteConfigData {

    private lateinit var remoteConfig: FirebaseRemoteConfig

    init {
        initRemoteConfig()
        syncConfigData()
    }

    private fun initRemoteConfig(): FirebaseRemoteConfig {
        remoteConfig = FirebaseRemoteConfig.getInstance()

        val configSettings: FirebaseRemoteConfigSettings = if (BuildConfig.DEBUG) {
            FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(0)
                .build()
        } else {
            FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(12 * 60 * 60)
                .build()
        }

        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(
            mapOf(remoteValues() to Gson().toJson(RemoteConfigModel()))
        ).addOnCompleteListener {
            Log.d(
                "FirebaseRemote",
                "Data ${remoteConfig.getString(remoteValues())}"
            )
        }.addOnFailureListener {
            Log.d("FirebaseRemote", "Failed")
        }.addOnCanceledListener {
            Log.d("FirebaseRemote", "Cancelled")
        }

        return remoteConfig
    }

    private fun remoteValues(): String {
        return if (BuildConfig.DEBUG) {
            "app_json"
        } else {
            "app_json"
        }
    }

    private fun syncConfigData() {
        var remoteConfigModel: RemoteConfigModel
        val firebaseRemoteConfig = FirebaseRemoteConfig.getInstance()

        firebaseRemoteConfig.fetchAndActivate()
            .addOnCompleteListener {
                remoteConfigModel = Gson().fromJson(
                    firebaseRemoteConfig.getString(remoteValues()),
                    RemoteConfigModel::class.java
                )
                Log.d(
                    "remote_data_pri",
                    remoteConfigModel.toString()
                )
            }.addOnSuccessListener {
                Log.d("remote_status", "Status is $it")
            }.addOnFailureListener {
                Log.d("remote_status", "Status is ${it.stackTrace}")
            }
    }
}

