package com.remote.configuration

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.remote.configuration.remote.FirebaseRemoteConfigData

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //With Json
        FirebaseRemoteConfigData()
        //With single Attribute
        checkForUpdate()
    }
    
    private fun checkForUpdate() {

        val appVersion: String = getAppVersionCode().toString()
        val remoteConfig = FirebaseRemoteConfig.getInstance()

        val latestVersion =
            remoteConfig.getString("latest_version_of_app")
        Log.e("Version", "appVersion $appVersion - latestVersion $latestVersion")

        if(checkMandateVersionApplicable(latestVersion, appVersion)) {
            onUpdateNeeded(true)
        } else {
            moveForward()
        }
    }

    private fun checkMandateVersionApplicable(
        latestVersion: String,
        appVersion: String
    ): Boolean {
        return try {
            val latestVersionInt = latestVersion.toInt()
            val appVersionInt = appVersion.toInt()
            latestVersionInt > appVersionInt
        } catch (exp: NumberFormatException) {
            false
        }
    }

    private fun getAppVersionCode(): Int {
        return BuildConfig.VERSION_CODE
    }

    private fun onUpdateNeeded(isMandatoryUpdate: Boolean) {
        val dialogBuilder = AlertDialog.Builder(this)
            .setTitle(getString(R.string.update_app))
            .setCancelable(false)
            .setMessage(if (isMandatoryUpdate) getString(R.string.dialog_update_available_message) else "A new version is found on Play store, please update for better usage.")
            .setPositiveButton(getString(R.string.update_now))
            { dialog, _ ->
                dialog?.dismiss()
                //OpenPlayStore
            }

        if (!isMandatoryUpdate) {
            dialogBuilder.setNegativeButton(getString(R.string.later)) { dialog, _ ->
                moveForward()
                dialog?.dismiss()
            }.create()
        }
        val dialog: AlertDialog = dialogBuilder.create()
        dialog.show()
    }

    private fun moveForward() {
        Toast.makeText(this, "Next Page Intent", Toast.LENGTH_SHORT).show()
    }

}