package com.remote.configuration

import android.app.Application
import android.util.Log
import com.google.firebase.FirebaseApp
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.firebase.remoteconfig.ktx.remoteConfig

class AppClass : Application() {
    override fun onCreate() {
        super.onCreate()
        initFirebaseRemoteConfig()

        val remoteConfig = Firebase.remoteConfig
        remoteConfig.setDefaultsAsync(R.xml.remote_config_default)
    }

    private fun initFirebaseRemoteConfig() {
        FirebaseApp.initializeApp(this)
        FirebaseRemoteConfig.getInstance().apply {
            //set this during development
            val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(5)
                .build()
            setConfigSettingsAsync(configSettings)
            //set this during development

            setDefaultsAsync(R.xml.remote_config_default)
            fetchAndActivate().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("TAG", "Config params updated: ${task.result}")
                } else {
                    Log.d("TAG", "Config params updated: ${task.result}")
                }
            }
        }

    }
}